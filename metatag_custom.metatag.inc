<?php
/**
 * Implements hook_metatag_info().
 */
function metatag_custom_metatag_info() {
  $info['groups']['custom_metatags'] = array(
    'label' => t('Custom Metatags'),
  );

  $info['tags']['custom_metatags'] = array(
    'label' => t('Custom Metatags'),
    'description' => t('Add any custom meta-tags'),
    'class' => 'DrupalTextMetaTag',
    'group' => 'custom_metatags',
    'form' => array(
      '#type' => 'textarea',
      '#rows' => 10,
      '#wysiwyg' => FALSE,
      '#description' => t('Use this format: custom_tag_name|custom_content -> This will create &lt;meta name="custom_tag_name" content="custom_content"&gt;'),
    ),
  );

  return $info;
}

/**
 * Implements hook_metatag_config_default_alter().
 */
function metatag_custom_metatag_config_default_alter(array &$configs) {

  $custom_tags = array(
    'custom_metatags' => array('value' => '')
  );

  foreach ($configs as &$config) {
    switch ($config->instance) {
      case 'global':
        $config->config += $custom_tags;
        break;

      case 'global:frontpage':
        $config->config += $custom_tags;
        break;

      case 'node':
        $config->config += $custom_tags;
        break;

      case 'taxonomy_term':
        $config->config += $custom_tags;
        break;

      case 'user':
        $config->config += array();
        break;
    }
  }
}